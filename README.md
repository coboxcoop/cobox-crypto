# cobox-crypto

Crypto primitives used in cobox

```
npm install cobox-crypto
```

## Usage

```
const crypto = require('cobox-crypto')
```

## API

```js
const address = crypto.address()
```

Returns a `ed25519` random 32 byte buffer

```js
const keyPair = crypto.keyPair()
```

Returns an `ed25519` keypair that can used for tree signing.

```js
const encKey = crypto.encryptionKey()
```

Returns an `ed25519` symmetric key used for shared secret encryption

```js
const accessKey = crypto.accessKey()

// OR

const accessKey = crypto.pack(address, encKey)
```

Returns an access key, which consists of an `ed25519` address, packed together with an `ed25519` symmetric key

```js
const keys = crypto.unpack(key)
```

Returns an object containing an address, and a shared secret if accessible. Address alone is used for blind replication. The shared secret can then be used for decryption.

```js
const { publicKey, secretKey } = crypto.boxKeyPair(seed)
```

Returns an `ed25519` private box keypair used for identification, message signing and encryption

```js
const boxed = box(publicKey, message, [context])
```

Encrypts a message to a given public key and returns it as a buffer
- `publicKey` buffer or hex encoded string
- `message` buffer or hex encoded string of any length
- `context`, if passed, will be hashed in to the shared secret. Should be a buffer or hex encoded string.

```js
const unboxed = unbox(cipherText, keypair, [context])
```

Decrypts a message using the given keypair.
- `cipherText` the encrypted message given as a buffer.
- `keypair` an object of the form `{ publicKey, secretKey }` both of which should be buffers or hex encoded strings.
- `context`, if given, will be hashed into the shared secret. Should be a buffer or hex encoded string.
